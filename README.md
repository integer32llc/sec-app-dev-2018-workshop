# Rust Workshop for SecAppDev 2018

## Setup

Before the workshop, please clone this repository locally and ensure
it builds. You may also wish to configure your editor to support Rust
syntax.

If you don't want to install Rust, you will be able to do most of the
exercises on the [Rust Playground][playground] ([alternate
hosting][i32playground]), but not the final set of exercises.

[playground]: https://play.rust-lang.org/
[i32playground]: https://play.integer32.com/

### Details

- [Download and install Rust 1.24.0][install].
- Clone this repository.
- Open the command line, change to this repository, and then execute `cargo build`.

If you see something ending in `Finished dev [unoptimized + debuginfo] ...`, you are good to go!

If you encounter errors, please either:

- open an issue here
- find me in person during the conference
- pop into the [rust-beginners IRC room][beginners]

We will get you up and running!

[install]: https://www.rust-lang.org/en-US/install.html
[beginners]: https://chat.mibbit.com/?server=irc.mozilla.org&channel=%23rust-beginners

### Optional

If you don't have an editor of choice, I would recommend giving [Visual
Studio Code][vscode] a shot, along with [the Rust extension][vscode-extension].

[vscode]: https://code.visualstudio.com/
[vscode-extension]: https://github.com/rust-lang-nursery/rls-vscode#quick-start
