#include <stdint.h>

typedef struct personS person_t;

person_t *person_new(char *name, uint8_t age);
void person_free(person_t *p);
char *person_name(person_t *p);
uint8_t person_age(person_t *p);
void person_get_older(person_t *p);
