#include "foo.h"

#include <stdlib.h>
#include <string.h>

struct personS {
  char *name;
  uint8_t age;
};

person_t *person_new(char *name, uint8_t age) {
  person_t *p = malloc(sizeof(person_t));
  p->name = strdup(name);
  p->age = age;
  return p;
}

void person_free(person_t *p) {
  free(p->name);
  free(p);
}

char *person_name(person_t *p) {
  return p->name;
}

uint8_t person_age(person_t *p) {
  return p->age;
}

void person_get_older(person_t *p) {
  p->age++;
}
