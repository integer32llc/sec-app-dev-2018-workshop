extern crate libc;

mod ffi {
    use libc;

    pub enum Person {}

    extern "C" {
        pub fn person_new(name: *const libc::c_char, age: libc::uint8_t) -> *mut Person;
    }
}

fn main() {
    println!("Program successfully compiled and run!");
}
